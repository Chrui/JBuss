package com.yuzi.common.result;

public class ErRes extends ComRes {
    private static final Integer code1 = 500;
    private static final Integer code2 = 404;
    private static final Integer code3 = 401;
    private static final Integer code4 = 301;

    public static ComRes Res(Object result){
        ComRes comRes = new ComRes(false,500,"失败");
        comRes.setResult(result);
        return comRes;
    }

}

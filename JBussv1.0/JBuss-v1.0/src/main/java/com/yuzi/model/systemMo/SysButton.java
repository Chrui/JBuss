package com.yuzi.model.systemMo;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.model.BaseMo.BaseModel;

import java.util.List;

public class SysButton extends BaseModel<SysButton> {

    public List<SysButton> findAllButtonByMenuId(Integer menuId){
        String sql = "select * from sys_button where menu_id = ? order by id";
        return this.find(sql,menuId);
    }

    public boolean addSysButton(SysButton sysButton){
        if (sysButton.getInt("id") == 0 || sysButton.getInt("id") == null){
            int maxIndex = getMaxIndex(this);
            sysButton.set("id",maxIndex+1);
        }
        return sysButton.save();
    }

    public int getMaxIndex(){
        return getMaxIndex(this);
    }

    public boolean delSysButton(Integer buttonId){
        return this.deleteById(buttonId);
    }

    public SysButton findButtonByButtonId(Integer buttonId){
        return this.findById(buttonId);
    }

    public boolean updateSysButtonByisHidden(Integer buttonId,String isHidden){
        return Db.update("UPDATE sys_button SET isHidden =? WHERE id=?",isHidden,buttonId)>0 ? true : false;
    }

    public boolean findSysBtnByBtnCodeAndRoleId(String btnCode,Integer roleId){
        String sql = "SELECT srb.* FROM sys_role_menu_button srb \n" +
                "\tINNER JOIN sys_button sb ON srb.button_id = sb.id \n" +
                "\tWHERE sb.ishidden = '0' AND srb.role_id = ? AND sb.code = ?";
        List<SysButton> sysButtonList = this.find(sql,roleId,btnCode);
        return sysButtonList!=null && sysButtonList.size()>0? true:false;
    }

    public List<Record> findSysRoleMenuBtnByRoleIdAndMenuId(Integer roleId,Integer menuId){
        String sql ="SELECT sb.name,sb.id,sb.code,sb.ishidden,srmb.role_id roleid FROM sys_button sb \n" +
                "\tLEFT JOIN sys_role_menu_button srmb ON sb.id = srmb.button_id AND srmb.role_id = ?\n" +
                "\tWHERE sb.menu_id = ? ORDER BY sb.id";
        return Db.find(sql,roleId,menuId);
    }
}

package com.yuzi.controller.systemCon;

import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.systemMo.SysMenu;
import com.yuzi.model.systemMo.SysRole;
import com.yuzi.model.systemMo.SysRoleMenuButton;
import com.yuzi.service.systemSer.*;

import java.util.List;
import java.util.Map;

public class RoleAuthController extends BaseController {

    private MenuService menuService;

    private SysRoleService sysRoleService;

    private SysRoleMenuButtonService sysRoleMenuButtonService;

    private SysButtonService sysButtonService;

    private SysRoleMenuService sysRoleMenuService;

    public void index(){
        render(Constant.PagePath.sysPage + "role-auth.html");
    }

    public void loadAllMenu(){
        //获取所有菜单
        Integer roleId = getParaToInt("roleId");
        List<Record> allSysMenu =  menuService.findAllMenu(roleId);
        renderJson(allSysMenu);
    }

    public void showRoleAuthCategory(){
        int roleId = getParaToInt();
        //获取角色
        SysRole sysRole = sysRoleService.getSysRoleById(roleId);
        setAttr("sysRole",sysRole);
        render(Constant.PagePath.sysPage + "role-auth-category.html");
    }

    public void  updateSysRoleMenuButton(){
         SysRoleMenuButton sysRoleMenuButton=getModel(SysRoleMenuButton.class,"rmb");
         String action=getPara("action");
         Map map=sysRoleMenuButtonService.updateSysRoleMenuButton(sysRoleMenuButton,action);
         if ("true".equals(map.get("flag"))){
            renderJson(SuRes.Res(map.get("msg")));
         }else{
            renderJson(ErRes.Res(map.get("msg")));
         }

    }


    public void showRoleAuthButton(){
        Integer menuId = getParaToInt(0);
        Integer roleId = getParaToInt(1);
        //获取角色
        SysMenu sysMenu = menuService.findSysMenuById(menuId);
        SysRole sysRole = sysRoleService.getSysRoleById(roleId);
        setAttr("sysMenu",sysMenu);
        setAttr("sysRole",sysRole);
        render(Constant.PagePath.sysPage + "role-auth-button.html");
    }
    public void showRoleAuthButtonParent(){
        Integer roleId = getParaToInt();
        SysRole sysRole = sysRoleService.getSysRoleById(roleId);
        setAttr("sysRole",sysRole);
        render(Constant.PagePath.sysPage + "role-auth-button-parent.html");
    }

    public void findSysRoleMenuBtnByRoleIdAndMenuId(){
        Integer menuId = getParaToInt("menuId");
        Integer roleId = getParaToInt("roleId");
        List<Record> sysRoleBtn = sysButtonService.findSysRoleMenuBtnByRoleIdAndMenuId(roleId,menuId);
        renderJson(sysRoleBtn);
    }
    public void stopRoleMenuByBothId(){
        int roleId = getParaToInt("roleId");
        int menuId = getParaToInt("menuId");

        int affectCount = sysRoleMenuService.deleteSysRoleMenuByBothId(roleId,menuId);
        if (affectCount == 1)
            renderJson(SuRes.Res("删除成功"));
        else{
            renderJson(ErRes.Res("删除失败"));
        }
    }

    public void startRoleMenuByBothId(){
        int roleId = getParaToInt("roleId");
        int menuId = getParaToInt("menuId");

        boolean isOk = sysRoleMenuService.addSysRoleMenuByBothId(roleId,menuId);
        if (isOk)
            renderJson(SuRes.Res("添加成功"));
        else{
            renderJson(ErRes.Res("添加失败"));
        }
    }
}

package com.yuzi.controller.systemCon;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Model;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.interceptor.LoginSessionInterceptor;
import com.yuzi.model.systemMo.SysDict;
import com.yuzi.model.systemMo.SysMenu;
import com.yuzi.model.systemMo.SysUser;
import com.yuzi.service.systemSer.HomeService;
import com.yuzi.service.systemSer.MenuService;
import com.yuzi.service.systemSer.SysDictService;
import com.yuzi.utils.UploadFileUtils;

import java.util.List;

@Before(LoginSessionInterceptor.class)
public class HomeController extends BaseController {

    private MenuService menuService;
    private HomeService homeService;
    private SysDictService sysDictService;

    public void index(){

        //加载菜单
        SysUser sysUser = this.getSessionAttr(Constant.SessionKey.userKey);
        List<SysMenu> allSysMenu =  menuService.initMenu(sysUser.get("username"));

        setAttr("allSysMenu",allSysMenu);
        render(Constant.PageUrl.indexPage);
    }

    public void welcome(){
        render(Constant.PagePath.sysPage + "welcome.html");
    }


    public void getSysDictByPcode() {
        String pcode = getPara("pcode");
        List<SysDict> sysDicts = sysDictService.findSysDictByPcode(pcode);
        renderJson(sysDicts);
    }

    //删除固定文件
    public void updateAnnex() {
        String classname = getPara("model");
        try {
            Class<?> clazz = Class.forName(classname);
            Model model = getModel((Class<? extends Model>)clazz,"mo");
            boolean isOk = homeService.updateAnnex(model);
            if (isOk) {
                renderJson(SuRes.Res("删除成功"));
            } else {
                renderJson(ErRes.Res("删除失败"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //删除未知名称文件
    public void updateColumn() {
        String classname = getPara("model");
        String column = getPara("column");
        try {
            Class<?> clazz = Class.forName(classname);
            Model model = getModel((Class<? extends Model>)clazz,"mo");
            boolean isOk = homeService.updateFile(model,column);
            if (isOk) {
                renderJson(SuRes.Res("删除成功"));
            } else {
                renderJson(ErRes.Res("删除失败"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }



    public void deleteFile(){
        String fileName = getPara("fileName");
        boolean isok = UploadFileUtils.deleteFile(fileName);
        if (isok) {
            renderJson(SuRes.Res("删除成功"));
        } else {
            renderJson(ErRes.Res("删除失败"));
        }
    }


}

package com.yuzi.controller.systemCon;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.common.Constant;
import com.yuzi.common.result.ErRes;
import com.yuzi.common.result.SuRes;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.model.systemMo.SysRole;
import com.yuzi.model.systemMo.SysUser;
import com.yuzi.service.systemSer.SysRoleService;
import com.yuzi.service.systemSer.SysUserService;

import java.util.List;
import java.util.Map;

public class UserController extends BaseController {

    private SysUserService sysUserService;

    private SysRoleService sysRoleService;

    public void index(){
        render(Constant.PagePath.sysPage + "user-list.html");
    }

    public void loadSysUserByPage(){
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        String userSele=getPara("userSele");
        String userNameSele=getPara("userNamesele");
        Page<SysUser> sysUserPage  = sysUserService.findAllSysUser(pageNumber,pageSize,userSele,userNameSele);
        renderJson(sysUserPage);
    }


    public void loadSysUserByRid(){
        int pageNumber = getParaToInt("pageNumber");
        int pageSize = getParaToInt("pageSize");
        int roleid=getParaToInt("roleid");
        Page<SysUser> sysUserPage  = sysUserService.findSysUserByroleId(pageNumber,pageSize,roleid);
        renderJson(sysUserPage);
    }


    public void findSysUserByroleId(){
        int rid =getParaToInt();
        setAttr("roleid",rid);
        render(Constant.PagePath.sysPage + "role-user-list.html");
    }


    public void findAllSysUser(){
        renderJson(sysUserService.findAllSysUser());
    }

    //批量删除用户
    public void batchDelSysUser() {
    	String param=getPara("param");
    	Map<String ,String> map=sysUserService.batchDelSysUser(param);
		if("false".equals(map.get("flag").toString()))
			renderJson(ErRes.Res(map.get("msg").toString()));
		else 
			renderJson(SuRes.Res(map.get("msg").toString()));
    }
    

    public void showUserAdd(){
        List<SysRole> sysRoleList = sysRoleService.findSysRoleByCondition(null);
        setAttr("sysRoleList",sysRoleList);
        render(Constant.PagePath.sysPage + "user-add.html");
    }

    public void showUserEdit(){
        int userId = getParaToInt();
        Record sysRecord = sysUserService.findUserAndRoleByUserId(userId);
        setAttr("user",sysRecord);
        List<SysRole> sysRoleList = sysRoleService.findSysRoleByCondition(null);
        setAttr("sysRoleList",sysRoleList);
        render(Constant.PagePath.sysPage + "user-edit.html");
    }

    public void addSysUser(){
        SysUser sysUser = getModel(SysUser.class,"user");
        int roleId = getParaToInt("roleId");
        Map<String ,String> map = sysUserService.addSysUser(sysUser,roleId);

        if ("true".equals(map.get("flag").toString()))
            renderJson(SuRes.Res(map.get("res").toString()));
        else{
            renderJson(ErRes.Res(map.get("res").toString()));
        }
    }

    public void deleteSysUser(){
        int id = getParaToInt("id");
        Map<String ,String> map = sysUserService.deleteSysUser(id);
        if ("true".equals(map.get("flag").toString())){
            renderJson(SuRes.Res(map.get("msg").toString()));
        }else{
            renderJson(ErRes.Res(map.get("msg").toString()));
        }
    }

    public void updateSysUser(){
        SysUser sysUser = getModel(SysUser.class,"user");
        int roleId = getParaToInt("roleId");
        Map<String ,String> map = sysUserService.updateSysUserAndRole(sysUser,roleId);
        if ("true".equals(map.get("flag").toString())){
            renderJson(SuRes.Res(map.get("msg").toString()));
        }else{
            renderJson(ErRes.Res(map.get("msg").toString()));
        }
    }

    public void updateUserInfo(){
        String oldPassword = getPara("oldPassword");
        String newPassword = getPara("newPassword");
        boolean flag = sysUserService.updateUserInfo(oldPassword,newPassword);
        if (flag){
            renderJson(SuRes.Res("修改成功"));
        }else{
            renderJson(ErRes.Res("修改失败，请核实原始密码是否正确"));
        }
    }

    //用户禁用/启用
    public void updateSysUserStatusById(){
        int userId = getParaToInt("userId");
        if (userId == Constant.SysConfig.sys_user_id){
            renderJson(ErRes.Res("不能禁用超级管理员账户！"));
            return;
        }
        String status = getPara("status");
        boolean isOk = sysUserService.updateSysUserStatusById(userId,status);
        if (isOk){
            renderJson(SuRes.Res("更新成功"));
        }else{
            renderJson(ErRes.Res("更新失败"));
        }
    }
}

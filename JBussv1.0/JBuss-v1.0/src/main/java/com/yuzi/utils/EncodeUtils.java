package com.yuzi.utils;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

import java.util.ArrayList;
import java.util.List;

public class EncodeUtils {

    public static List<String> encodedPassword(String...strings){
        List<String> list = new ArrayList<>();
        //此处我们使用MD5算法，“密码+盐（用户名+随机数）”的方式生成散列值
        String salt = new SecureRandomNumberGenerator().nextBytes().toHex();
        int hashIterations = 2;

        SimpleHash hash = new SimpleHash("md5",strings[1],strings[0] + salt ,hashIterations);
        list.add(strings[0] + salt);
        list.add(hash.toHex());
        return list;
    }
}

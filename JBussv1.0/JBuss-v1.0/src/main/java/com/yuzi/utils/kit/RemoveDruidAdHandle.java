package com.yuzi.utils.kit;

import com.alibaba.druid.util.Utils;
import com.jfinal.handler.Handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * 删除Druid 广告代码
 */
public class RemoveDruidAdHandle extends Handler {
    private String visitPath = "/druid/js/common.js";

    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        // TODO Auto-generated method stub
        if (target.equals(visitPath)) {
            isHandled[0] = true;
            try {
                String text = Utils.readFromResource("support/http/resources/js/common.js");
                text = text.replaceAll("<a.*?banner\"></a><br/>", "");
                text = text.replaceAll("powered.*?shrek.wang</a>", "");
                response.setContentType("text/javascript;charset=utf-8");
                response.getWriter().write(text);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            next.handle(target, request, response, isHandled);
        }

    }
}
